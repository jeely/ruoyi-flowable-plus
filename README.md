## 平台简介
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitee.com/JavaLionLi/RuoYi-Vue-Plus/blob/master/LICENSE)
[![使用IntelliJ IDEA开发维护](https://img.shields.io/badge/IntelliJ%20IDEA-提供支持-blue.svg)](https://www.jetbrains.com)
[![Spring Boot](https://img.shields.io/badge/Spring%20Boot-2.5-blue.svg)]()
[![JDK-8+](https://img.shields.io/badge/JDK-8-green.svg)]()
[![JDK-11](https://img.shields.io/badge/JDK-11-green.svg)]()

- 本项目基于 [RuoYi-Vue-Plus](https://gitee.com/JavaLionLi/RuoYi-Vue-Plus) 进行二次开发扩展，整合了 [RuoYi-flowable](https://gitee.com/tony2y/RuoYi-flowable) 工作流功能（Flowable）并对代码进行修改。流程设计页面采用 [bpmn-process-designer](https://gitee.com/MiyueSC/bpmn-process-designer) 开源项目。
- 本项目主要针对`Flowable`工作流场景开发，系统其他功能同步升级 [RuoYi-Vue-Plus](https://gitee.com/JavaLionLi/RuoYi-Vue-Plus)。
- 采用`MIT开源协议`，完全免费给个人及企业使用。
- 项目处于初期开发阶段，基础的工作流流程能够跑通，但存在很多不足。因此，目前仅推荐用于学习、毕设等个人使用。


## 参考文档
- 项目文档：[RuoYi-Flowable-Plus开发文档](https://gitee.com/KonBAI-Q/ruoyi-flowable-plus/wikis/%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D)
- 项目基于 [RuoYi-Vue-Plus](https://gitee.com/JavaLionLi/RuoYi-Vue-Plus) 扩展开发，因此可以直接参考 [RuoYi-Vue-Plus文档](https://gitee.com/JavaLionLi/RuoYi-Vue-Plus/wikis/pages)。

## 贡献代码

欢迎各路英雄豪杰 `PR` 代码 请提交到 `develop` 开发分支 统一测试发版

### 其他

* Ruoyi-Flowable-Plus交流群：[![加入QQ群](https://badg.now.sh/badge/icon/1007207992?icon=qq&label=QQ)](https://jq.qq.com/?_wv=1027&k=PYDZa1tA)


## 演示图例
<table style="width:100%; text-align:center">
<tbody>
<tr>
  <td>
    <span>登录页面</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/164043_74b57010_5096840.png" alt="登录页面"/>
  </td>
  <td>
    <span>用户管理</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/164236_2de3b8da_5096840.png" alt="用户管理"/>
  </td>
</tr>
<tr>
  <td>
    <span>流程分类</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/164839_ca79b066_5096840.png" alt="流程分类"/>
  </td>
  <td>
    <span>流程表单</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/165118_688209fd_5096840.png" alt="流程表单"/>
  </td>
</tr>
<tr>
  <td>
    <span>流程定义</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/165916_825a85c8_5096840.png" alt="流程定义"/>
  </td>
  <td>
    <span>流程发起</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/171409_ffb0faf3_5096840.png" alt="流程发起"/>
  </td>
</tr>
<tr>
  <td>
    <span>表单设计</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/172933_7222c0f2_5096840.png" alt="表单设计"/>
  </td>
  <td>
    <span>流程设计</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/165827_44fa412b_5096840.png" alt="流程设计"/>
  </td>
</tr>
<tr>
  <td>
    <span>发起流程</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/171651_4639254b_5096840.png" alt="发起流程"/>
  </td>
  <td>
    <span>代办任务</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/171916_7ba22063_5096840.png" alt="代办任务"/>
  </td>
</tr>
<tr>
  <td>
    <span>任务办理</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/172204_04753399_5096840.png" alt="任务办理"/>
  </td>
  <td>
    <span>流转记录</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/172350_179e8341_5096840.png" alt="流转记录"/>
  </td>
</tr>
<tr>
  <td>
    <span>流程跟踪</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/172547_fe7414d4_5096840.png" alt="流程跟踪"/>
  </td>
  <td>
    <span>流程完结</span>
    <img src="https://images.gitee.com/uploads/images/2022/0424/173159_8cc57e74_5096840.png" alt="流程完结"/>
  </td>
</tr>
</tbody>
</table>
